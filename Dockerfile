FROM x11docker/xfce

# agregamos listas non-free porque stremio tiene dependencias non-free
RUN echo 'deb http://ftp.de.debian.org/debian buster main non-free' >> /etc/apt/sources.list

# copiamos archivos necesarios
COPY stremio_4.4.120-1_amd64.deb /stremio_4.4.120-1_amd64.deb
COPY libx264-152_0.152.2854+gite9a5903-2_amd64.deb /libx264-152_0.152.2854+gite9a5903-2_amd64.deb

# updateamos listas e instalamos utilidades de debugeo por si acaso
RUN apt-get update
RUN apt-get install -y curl nano

# instalamos paquetes
RUN apt-get install -yf /stremio_4.4.120-1_amd64.deb
RUN apt-get install -yf /libx264-152_0.152.2854+gite9a5903-2_amd64.deb
