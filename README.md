Este proyecto sirve para correr stremio dentro de un container de Docker pero se puede usar para correr otras aplicaciones gráficas también (como firefox usando el paquete firefox-esr o telegram usando el paquete telegram-desktop, etc.).

Primero instalamos x11docker localmente con el administrador de paquetes de tu distro o como dice en
https://github.com/mviereck/x11docker#installation

Vamos a usar esta imagen de XFCE
https://hub.docker.com/r/x11docker/xfce
(https://github.com/mviereck/dockerfile-x11docker-xfce)

Para probarla rápido localmente:

`x11docker --desktop --size 320x240 x11docker/lxde`

Para usar el Dockerfile de este proyecto necesitamos descargar stremio de https://www.stremio.com/downloads:

`curl -O https://dl.strem.io/shell-linux/v4.4.120/stremio_4.4.120-1_amd64.deb`

También la librería x264 para solucionar un problema que ocurre como indica en
https://www.how2shout.com/linux/how-to-install-stremio-app-on-linux-mint-distro/:

`wget http://archive.ubuntu.com/ubuntu/pool/universe/x/x264/libx264-152_0.152.2854+gite9a5903-2_amd64.deb`

Una vez descargados los archivos chequeamos que los nombres sean los mismos que figuran en el Dockerfile.
Una vez con estos 2 archivos en la raíz del proyecto compilamos la imagen:

`docker build -t docker-xfce-stremio .`

Y la corremos con x11docker:

`x11docker --desktop --size 800x600 docker-xfce-stremio`

(acá usamos la resolución 800x600 pero puede ser 1024x768 o cualquiera que quieran)

Además podemos ver el container andando:

`docker ps`

O conectarnos a su terminal (corregir el nombre del container acorde):

`docker exec -it x11docker_X106_docker-xfce-stremio_16859749722 bash`

Si queremos logearnos como root dentro del container primero hay que pasarle `--sudouser` al comando x11docker.
Después ejecutar `su` dentro del container, contraseña "x11docker".

Tener en cuenta que cualquier modificación hecha al container una vez abierto va a ser descartada cuando cierren la ventana del XFCE o corten la ejecución del comando x11docker.

Para más imágenes de desktops environments que no sean XFCE visitar https://hub.docker.com/u/x11docker/
